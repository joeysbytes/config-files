######################################################################
# Custom bash prompt
######################################################################
PS1="\[\033[37;44m\][\[\033[32m\]\u\[\033[37m\]@\[\033[93;45m\]\h\[\033[32;44m\] \D{%a} \[\033[37m\]\D{%Y-%m-%d} \[\033[32m\]\D{%H:%M:%S}\[\033[37m\]] \[\033[30;46m\]\w\[\033[37;40m\]\n\[\033[33;40m\]\$\[\033[0m\] "

######################################################################
# Utility aliases
######################################################################
alias ll='ls -l'
alias llh='ls -lh'
alias lla='ls -la'
alias lld='ls -ld'
alias psg='ps -ef|grep -i'
alias up='cd ..'
alias dfh='df -Th -x tmpfs -x devtmpfs -x squashfs'
if which docker>/dev/null 2>&1;
then
    alias dockerprune='docker image prune --force;docker volume prune --force'
fi

######################################################################
# Folder aliases
######################################################################
if [ -e ~/Downloads ]; then alias cddl='cd ~/Downloads'; fi;
if [ -e ~/Documents ]; then alias cddoc='cd ~/Documents'; fi;
if [ -e ~/Development ]; then alias cddev='cd ~/Development'; fi;
if [ -e ~/Music ]; then alias cdmus='cd ~/Music'; fi;
if [ -e ~/Pictures ]; then alias cdpic='cd ~/Pictures'; fi;
if [ -e ~/Videos ]; then alias cdvid='cd ~/Videos'; fi;
if [ -e ~/bin ]; then alias cdbin='cd ~/bin'; fi;
if [ -e ~/Dropbox ]; then alias cddrop='cd ~/Dropbox'; fi;
if [ -e ~/Nextcloud ]; then alias cdnext='cd ~/Nextcloud'; fi;
